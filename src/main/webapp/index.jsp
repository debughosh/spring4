	<!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<body>
<div ng-app="myApp" ng-controller="myCtrl">
<ul>
  <li ng-repeat="x in employees">
    {{ x.name + ', ' + x.age }}
  </li>
</ul></div>
<script>
	var myApp= angular.module('myApp',[]);
	myApp.controller('myCtrl',function($scope, $http){
		$http.get("http://localhost:8080/spring4/employee/getEmployees").then(function(response){
		//alert(response.data);
			$scope.employees = response.data.employees;
		});
	});
</script>

</body>
</html>
