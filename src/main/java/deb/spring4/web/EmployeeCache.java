/**
 * 
 */
package deb.spring4.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import deb.spring4.web.domain.Employee;

/**
 * @author GHOSH
 *
 */
@Component
public class EmployeeCache {
	private Map<String, Employee> empCache = new HashMap<>();
	
	public void addEmployee(Employee emp){
		empCache.put(emp.getName(),emp);
	}
	
	public Employee getEmployee(String name){
		return empCache.get(name);
	}
	
	public List<Employee> getEmployees(){
		return new ArrayList<>(empCache.values());
	}
	
	public void deleteEmployee(String employeeName){
		empCache.remove(employeeName);
	}
}
