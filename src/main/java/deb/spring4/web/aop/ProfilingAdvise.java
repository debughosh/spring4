/**
 * 
 */
package deb.spring4.web.aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author GHOSH
 *
 */
@Aspect
@Component
//@Order(2)
public class ProfilingAdvise {
	
	private ThreadLocal<Stats> threadLocalStats;
	
	public ProfilingAdvise(){
		threadLocalStats = new ThreadLocal<>();
	}

	@Pointcut("execution(* deb.spring4.web.BasicController.*(..))")
	public void profilingPointCut(){
		
	}
	@Before("profilingPointCut()")
	public void beforeMethodCalls(){
		Stats currentThreadStats = new Stats(System.currentTimeMillis(),Thread.currentThread().getName());
		threadLocalStats.set(currentThreadStats);
	}
	
	@After("profilingPointCut()")
	public void afterMethodCall(){
		Stats currentThreadStats = threadLocalStats.get();
		System.out.println("Execution time for Thread "+currentThreadStats.getThreadName()+" is "+(System.currentTimeMillis()-currentThreadStats.getStartTime()));
	}
	
}

class Stats{
	private long startTime;
	private String threadName;
	public Stats(long currentTimeArg, String threadName){
		startTime = currentTimeArg;
		this.threadName = threadName;
	}
	
	public long getStartTime(){
		return startTime;
	}
	
	public String getThreadName(){
		return threadName;
	}
}