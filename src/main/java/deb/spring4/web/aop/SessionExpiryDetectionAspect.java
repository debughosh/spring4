
/**
 * 
 */
package deb.spring4.web.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
//@Order(1)
public class SessionExpiryDetectionAspect {
	
	@Autowired
	 HttpServletRequest request;
	
	/*
	 * Needed to bind this to an annotation representing @Controller to ensure
	 * the webContext is created and session is ready when this aspect is called.
	 */
	@Before("within(@org.springframework.web.bind.annotation.RestController *)")
	public void checkSessionExpiry(){
		HttpSession session = request.getSession();
		long timeout = session.getLastAccessedTime()+(session.getMaxInactiveInterval()*1000);
		System.out.println("$$$$$$$$$$$$$$$$ HttpSession is "+session);
		if(System.currentTimeMillis()>timeout){
			System.out.println("Timeout happened !!");
		}else{
			System.out.println("Session Active @@@@@@@@");
		}
	}
	
}
