/**
 * 
 */
package deb.spring4.web.aop;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackages={"deb.spring4.web"})
public class ExceptionAdvise {
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public void printException(Exception e){
		System.out.println("The exceptoin thrown is @@@@@@@@@@@@"+e);
		e.printStackTrace();
	}
}
