package deb.spring4.web.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import deb.spring4.web.domain.Employee;
import deb.spring4.web.domain.Employees;

public class EmployeeRestServiceClient {
	public static void main(String [] args){
		
		Employee empTemp = new Employee();
		empTemp.setName("Saurav Ganguly");
		empTemp.setAge(44);
		empTemp.setSalary(100000.01f);
		System.out.println(empTemp);
		createEmployee(empTemp);
		empTemp = new Employee();
		empTemp.setName("Sachin Tendulkar");
		empTemp.setAge(43);
		empTemp.setSalary(110000.00f);
		System.out.println(empTemp);
		createEmployee(empTemp);
		empTemp = new Employee();
		empTemp.setName("Rahul Dravid");
		empTemp.setAge(43);
		empTemp.setSalary(100000.00f);
		System.out.println(empTemp);
		createEmployee(empTemp);
		empTemp = new Employee();
		empTemp.setName("Virat Kohli");
		empTemp.setAge(29);
		empTemp.setSalary(100000.00f);
		System.out.println(empTemp);
		createEmployee(empTemp);
		
		getEmployees();
		//restTemplate.getForObject("http://localhost:8080/spring4/employee/getEmployees", responseType, urlVariables)
	}
	
	
	private static void createEmployee(Employee emp){
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Employee> httpEntity = new HttpEntity<Employee>(emp, headers);
		restTemplate.postForObject("http://localhost:8080/spring4/employee/createEmployee", httpEntity, Void.class);
		System.out.println("Successfully created Employee");
		//restTemplate.g
	}
	
	private static void getEmployees() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON));

		HttpEntity<String> entity = new HttpEntity<>(null, headers);

		ResponseEntity<Employees> responseEntity= restTemplate.exchange("http://localhost:8080/spring4/employee/getEmployees", HttpMethod.GET, entity, Employees.class);
		
		Employees employees = responseEntity.getBody();
		List<Employee> listEmployees = employees.getEmployees();
		for(Employee employee : listEmployees){
			System.out.println(employee);
		}
		 //MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<String, String>();
		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("name", "sachin Tendulkar");
		//HttpEntity<MultiValueMap<String, String>>entity2 = new HttpEntity<>(requestParams,headers);
		//ResponseEntity<Employee> responseEntityEmp= restTemplate.exchange("http://localhost:8080/spring4/employee/getEmployee", HttpMethod.GET, entity2, Employee.class);
		//Employee emp = responseEntityEmp.getBody();
		Employee emp = restTemplate.getForObject("http://localhost:8080/spring4/employee/getEmployee/Saurav Ganguly", Employee.class);
		
			System.out.println(emp);
	}
}
