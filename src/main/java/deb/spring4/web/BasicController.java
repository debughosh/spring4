 	/**
 * 
 */
package deb.spring4.web;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import deb.spring4.web.domain.Employee;
import deb.spring4.web.domain.Employees;
/**
 * @author GHOSH
 *
 */
@RestController
@RequestMapping("/employee")
//@PreAuthorize("hasRole('ROLE_ABC')")
public class BasicController {

	@Autowired
	EmployeeCache empCache;
	
	@RequestMapping("/greet")
	
	public String greet(){
		return "hi there!";
	}
	//@RequestMapping(value="/getEmployees",produces={"application/json; charset=UTF-8"})
	@RequestMapping(value="/getEmployees",method = RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public Employees getEmployees(){
		List<Employee> employeesList = new ArrayList<Employee>();
		Employee employee = new Employee("Virat",29,200000f);
		employeesList.add(employee);
		employee = new Employee("Rohit",31,150000f);
		employeesList.add(employee);
		employee = new Employee("Dhoni",35,230000f);
		employeesList.add(employee);		
		Employees employees = new Employees();
		if(CollectionUtils.isEmpty(empCache.getEmployees()))
			employees.setEmployees(employeesList);
		else
			employees.setEmployees(empCache.getEmployees());
		System.out.println("Emp list is "+empCache.getEmployees());
		return employees;
	}
	
	@RequestMapping(value="/getEmployee/{name}",method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public Employee getEmployee(@PathVariable String name){
		System.out.println("Employee searched is "+name);
		return empCache.getEmployee(name);
	}
	
	@RequestMapping(value="/createEmployee",method=RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public void createEmployee(@RequestBody @Valid Employee arg){
		
		empCache.addEmployee(arg);
		System.out.println(arg+" created!");
	}
	
	@RequestMapping(value="/deleteEmployee",method=RequestMethod.DELETE,consumes={MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public void deleteEmployee( String name){
		empCache.deleteEmployee(name);
	}
}
