/**
 * 
 */
package deb.spring4.web.domain;

import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author GHOSH
 *
 */
//@JsonSerialize(using=EmployeeSerializer.class)
@XmlRootElement(name="EmployeeDetails")
public class Employee {
	//@JsonProperty("goodName")
	@Pattern(regexp="^[A-Z][a-z]+\\s[A-Z][a-z]+")
	private String name;
	private int age;
	private float salary;
	
	public Employee(){
		
	}
	
	public Employee(String name,int age , float salary){
		this.name=name;
		this.age=age;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Name:"+this.getName()+" Age:"+this.getAge()+" Salary:"+this.getSalary();
	}

}
