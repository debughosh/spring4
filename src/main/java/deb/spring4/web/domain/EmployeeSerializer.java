/**
 * 
 */
package deb.spring4.web.domain;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * @author GHOSH
 *
 */
public class EmployeeSerializer extends JsonSerializer<Employee> {

	@Override
	public void serialize(Employee emp, JsonGenerator jsonGen, SerializerProvider sp)
			throws IOException, JsonProcessingException {
		// TODO Auto-generated method stub
		jsonGen.writeStartObject();
		jsonGen.writeStringField("employeeName",emp.getName());
		jsonGen.writeNumberField("employeeAge",emp.getAge());
		jsonGen.writeNumberField("employeeRenumeration", emp.getSalary());
		jsonGen.writeEndObject();
		
	}

}
