/**
 * 
 */
package deb.spring4.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import deb.spring4.web.aop.ProfilingAdvise;

/**
 * @author GHOSH
 *
 */
@Configuration

@EnableWebMvc
@EnableAspectJAutoProxy
@ComponentScan("deb.spring4.web")
public class ApplicationConfiguration {
	
	
	// TODO need to figure out how to do spring boot.
	public static void main(String [] args){
		//SpringApplication.run(ApplicationConfiguration.class,args);
		Pattern pattern = Pattern.compile("^[A-Z][a-z]+\\s[A-Z][a-z]+");
		Matcher matcher = pattern.matcher("Saurav Ganguly");
		System.out.println(matcher.matches());
	}
}
