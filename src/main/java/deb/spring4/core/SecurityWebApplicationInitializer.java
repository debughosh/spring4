/**
 * 
 */
package deb.spring4.core;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author GHOSH
 * 
 * This empty class ensures that all the security related filters are registered for every
 * url.
 *
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
