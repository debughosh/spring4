/**
 * 
 */
package deb.spring4.web;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import deb.spring4.web.domain.Employee;

/**
 * @author debas
 *
 */
public class EmployeeCacheTest {
	
	private EmployeeCache underTest;
	
	@Before
	public void setUp(){
		underTest = new EmployeeCache();
	}
	
	@Test
	public void testEmployeeCacheCrud(){
		underTest.addEmployee(new Employee("Juan Martin", 29, 50000.0f));
		assertEquals(1, underTest.getEmployees().size());
		assertEquals(29,underTest.getEmployee("Juan Martin").getAge());
	}

}
